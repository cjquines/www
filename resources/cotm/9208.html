<!--#include virtual="../../head-pretitle.html" -->
<!--#set var="this" value="9208" -->
<title>Call of the Month - August, 1992</title>

<!--#include virtual="header.html" -->
<!--#include virtual="../../head-posttitle.html" -->
<div id="main">

<div id="cotmhead">
<h1>Call of the Month: August, 1992</h1>
<h2>Recovering from mistakes</h2>
<p id="author">by Barry Leiba</p>
</div>
<hr class="nocss" />
<div id="cotmmain">
All dancers, new and experienced, make mistakes from time to time.
Sometimes the dancer can correct the mistake immediately. At other
times, someone else in the square (or even someone in another
square!) can help, or the other dancers can leave a hole for the
lost dancer to find and occupy. The jocular <q>call</q> <span class="call">GO OVER THERE
AND TURN AROUND</span> (abbreviated "<span class="call">GOTATA</span>") is often used in
such situations. Nevertheless, despite all efforts to keep things
going, squares occasionally break down.
<p>Since our aim is to dance, the immediate goal of a broken square
is to resume dancing as quickly as possible. This month, we'll look
at some ways to do that. We'll use some concepts that we saw in
earlier columns about formations and formation awareness. We'll
also look at ways you can help keep your square from breaking down
in the first place.</p>
<p>As an individual dancer, you must realize when you're lost or
mistaken, and accept help when it's given. Always listen carefully
to the caller, who will often give you hints or cue you through the
difficult parts. For instance, a common mistake when doing the call
<span class="call">EXTEND</span> from a left-handed setup is to switch hands and make
a right-handed setup as you go. The caller will often say
"<span class="call">EXTEND</span> to a left-hand wave" in order to avert that error;
if you're listening, you'll have no problem.</p>
<p>Another individual thing to watch is body flow. Square
<i>dancing</i> ought to have smooth body flow, and most of the time
it does. If you don't know where to go, chances are you should keep
moving <b>slowly</b> in the direction that you've been going. If
you were turning to the right, keep turning to the right. By
slowing down, you give someone else a chance to help. In general,
if you're lost, don't turn around and wander off in another
direction&mdash;abrupt changes of direction are occasionally right,
but more often they're wrong and will get you even more lost.</p>
<p>If you know that you're lost and there's no one to help you,
look for the square's symmetry. Unless the caller has done some
asymmetric directional stuff (like having only one couple
half-sashay, or having <q>the line nearest the caller</q> do something
different) square dancing always retains symmetry even if the
caller makes a mistake! If you're a head boy, <b>know who the other
head boy is</b>. You will always be opposite him in the square; if
he's on the end of one line looking in, you'll be on the other end
of the other line looking in. If he's number one in a column,
you'll be number one in the other side of the column. Um... unless,
of course, he's made a mistake too. You will always be diagonally
opposite from your symmetry, and you'll always be facing the
opposite wall&mdash;if he's facing the caller, you'll be facing the
back wall. So, if you're lost and everyone else is still going,
look for the hole and get there. If you can, find your symmetry and
you'll know where you need to be and which way you need to
face.</p>
<p>Another thing that will help your square is to <b>avoid
chatter</b>. This goes along with the advice to listen to the
caller: if you (or someone else in your square) is talking, then
you can't pay attention to the caller's cues and hints, nor indeed,
even to the calls themselves. If you have to help someone verbally,
do it with a brief word or two. "Over here.", "Turn around.", and
"Follow Allison." are usually better bets than something wordy.
Don't analyze mistakes during the tip. As Kenny Rogers sings in
<i>The Gambler</i>, there'll be time enough for countin' when the
dealin's done.</p>
<p>Well, you've done all that and your square has broken down
despite it all? The most obvious way to get back into the dance
when your square breaks down is to <q>go home,</q> return to a static
square, and wait for the caller to get the rest of the dancers home
too. You can then start the next sequence fresh. The trouble with
this is that the sequence might be long, and you might be standing
for some time before you can start dancing again. There are better
ways than waiting it out.</p>
<p>The first rule of thumb to use when your square is broken is to
<b>make lines</b>. The best thing, generally, is to make <q>normal</q>
lines (couples with the <q>boys</q> on the left and the <q>girls</q> on the
right) and to align them with a wall. It doesn't matter whether
you're with your partner or whether the lines are parallel to the
head or side walls. Then you can watch the rest of the floor and
when they're in facing lines, you can continue.</p>
<p>Some callers don't get the formation back into facing lines very
often either, though, especially at levels above Plus. You can get
even better results by knowing the basic formations. When you hear
<span class="call">FERRIS WHEEL</span>, if you can immediately get into <span class="call">STARTING
DOUBLE-PASS-THRU</span> formation, then after everyone else has done
the <span class="call">FERRIS WHEEL</span> you can continue from there. Make normal
couples and worry about the sexes later (at Mainstream and Plus
you'll most likely be right anyway). Similarly, get to know other
calls and formations. If you hear <span class="call">SWING THRU</span> or <span class="call">SPIN
CHAIN THRU</span> or <span class="call">RELAY THE DEUCEY</span>, make waves so you'll be
ready for the <b>next</b> call&mdash;right-handed waves will be
correct most of the time; if you're watching the floor you can make
left-handed waves when necessary (and perhaps you can even get the
sexes right if you know whether the boys are in the middle, for
instance). The more aware you are of formations, the better the
chances are that you can get your square dancing again.</p>
<p>Now, let's go back a little bit: one good way to <b>make</b>
these formations is by going back home to a static square! Then if
you need facing lines, the heads can bend to make lines with the
couple to their right (who just slide over without turning). If you
need <span class="call">DOUBLE-PASS-THRU</span>, then heads can wheel into the center
to face each other. For waves, the heads can lead right and make
waves with the sides. Note that it's always the <b>heads</b> taking
the action. That avoids the problem of having perpendicular lines
because no one was in charge. If you get squared up, someone who
knows what formation is needed announces the formation, and the
heads take the required action, you should be able to get back into
things and keep dancing.</p>
<p>One more tip to keep the square going; I mentioned it a bit
earlier: <b>Always align your formation with the walls.</b> When
you get back in after a mistake, when you're promenading and the
caller has one couple <span class="call">WHEEL AROUND</span> or <span class="call">BACKTRACK</span>, or
any other time you're making some kind of line or column, be sure
it's parallel to a wall. Formations that are angled are confusing,
and it's easy to get lost, forget how far you've turned, and so on.
Unless there's a good reason to do otherwise (and there seldom is),
line up with the walls.</p>
<p>Finally, try to learn to continue dancing even when you know
your square isn't right. That's easy when the only mistake is that
two girls are switched. It's a bit harder if a boy and a girl are
switched and they don't know it, or they aren't able to dance the
other part. That's one reason that it's useful to be <q>bidansual</q> --
if you get your sex switched in the middle of a tip, you can keep
dancing and fix it later. Even if you're not bidansual, you can
often continue dancing in the wrong role long enough for the caller
to get you home.</p>
<p>So there are some things to think about: listen to the caller,
accept help from others, look for the empty spot, understand
symmetry, trust the body flow, be aware of formations, align your
formation with the walls, and learn to be able to keep on dancing
after a sex change. And isn't dancing what we're here for, after
all?</p>


</div><!-- cotmmain -->
<p id="cotmprint">(<a href="resources/cotm/9208.pdf">Printable Version</a>)</p>
<!--#include virtual="footer.html" -->
</div> <!-- main -->
<!--#include virtual="../../footer.html" -->
